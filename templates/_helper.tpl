{{/* vim: set filetype=mustache: */}}

{{- define "cassandra.seeds" -}}
{{- $seeds := list }}
{{- $fullname := include "generate.fullname" . }}
{{- $namespace := .Release.Namespace }}
{{- $domain := .Values.clusterDomain }}
{{- $seedCount := .Values.seedCount | int }}
{{- range $e, $i := until $seedCount }}
{{- $seeds = append $seeds (printf "%s-%d.%s-headless.%s.svc.%s" $fullname $i $fullname $namespace $domain) }}
{{- end }}
{{- join "," $seeds }}
{{- end -}}
