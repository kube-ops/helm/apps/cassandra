# cassandra

[![Version: 0.1.4](https://img.shields.io/badge/Version-0.1.4-informational?style=flat-square) ](#)
[![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ](#)
[![AppVersion: 3.11.9](https://img.shields.io/badge/AppVersion-3.11.9-informational?style=flat-square) ](#)
[![Artifact Hub: kube-ops](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/kube-ops&style=flat-square)](https://artifacthub.io/packages/helm/kube-ops/cassandra)

The Apache Cassandra database is the right choice when you need scalability and high availability without compromising performance

## Installing the Chart

To install the chart with the release name `my-release`:

```console
$ helm repo add kube-ops https://charts.kube-ops.io
$ helm repo update
$ helm upgrade my-release kube-ops/cassandra --install --namespace my-namespace --create-namespace --wait
```

## Uninstalling the Chart

To uninstall the chart:

```console
$ helm uninstall my-release --namespace my-namespace
```

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` | Affinity for pod assignment ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity |
| clusterDomain | string | `"cluster.local"` |  |
| clusterName | string | `"Cassandra Cluster"` | Cluster name |
| dc | string | `"dc1"` |  |
| deploymentType | string | `"StatefulSet"` |  |
| dnsConfig | object | `{}` | DNS configuration for all replicas |
| dnsPolicy | string | `""` |  |
| extraArgs | list | `[]` | Additional arguments |
| extraEnvVars | list | `[]` | Additional environment variables |
| extraVolumeMounts | list | `[]` | Additional mounts for application |
| extraVolumes | list | `[]` | Additional volumes for application |
| fullnameOverride | string | `""` | Overrides the full name |
| global.imagePullPolicy | string | `"IfNotPresent"` | Image download policy ref: https://kubernetes.io/docs/concepts/containers/images/#updating-images |
| global.imagePullSecrets | list | `[]` | List of the Docker registry credentials ref: https://kubernetes.io/docs/concepts/containers/images/#updating-images |
| hostAliases | list | `[]` | Adding entries to Pod /etc/hosts with HostAliases ref: https://kubernetes.io/docs/concepts/services-networking/add-entries-to-pod-etc-hosts-with-host-aliases/ |
| image.repository | string | `"docker.io/library/cassandra"` | Overrides the image repository |
| image.tag | string | `""` | Overrides the image tag whose default is the chart appVersion. |
| initContainers | list | `[]` |  |
| jvmOptions[0] | string | `"-ea"` |  |
| jvmOptions[10] | string | `"-XX:+UseNUMA"` |  |
| jvmOptions[11] | string | `"-XX:+PerfDisableSharedMem"` |  |
| jvmOptions[12] | string | `"-Djava.net.preferIPv4Stack=true"` |  |
| jvmOptions[13] | string | `"-XX:+UseParNewGC"` |  |
| jvmOptions[14] | string | `"-XX:+UseConcMarkSweepGC"` |  |
| jvmOptions[15] | string | `"-XX:+CMSParallelRemarkEnabled"` |  |
| jvmOptions[16] | string | `"-XX:SurvivorRatio=8"` |  |
| jvmOptions[17] | string | `"-XX:MaxTenuringThreshold=1"` |  |
| jvmOptions[18] | string | `"-XX:CMSInitiatingOccupancyFraction=75"` |  |
| jvmOptions[19] | string | `"-XX:+UseCMSInitiatingOccupancyOnly"` |  |
| jvmOptions[1] | string | `"-XX:+UseThreadPriorities"` |  |
| jvmOptions[20] | string | `"-XX:CMSWaitDuration=10000"` |  |
| jvmOptions[21] | string | `"-XX:+CMSParallelInitialMarkEnabled"` |  |
| jvmOptions[22] | string | `"-XX:+CMSEdenChunksRecordAlways"` |  |
| jvmOptions[23] | string | `"-XX:+CMSClassUnloadingEnabled"` |  |
| jvmOptions[24] | string | `"-XX:+PrintGCDetails"` |  |
| jvmOptions[25] | string | `"-XX:+PrintGCDateStamps"` |  |
| jvmOptions[26] | string | `"-XX:+PrintHeapAtGC"` |  |
| jvmOptions[27] | string | `"-XX:+PrintTenuringDistribution"` |  |
| jvmOptions[28] | string | `"-XX:+PrintGCApplicationStoppedTime"` |  |
| jvmOptions[29] | string | `"-XX:+PrintPromotionFailure"` |  |
| jvmOptions[2] | string | `"-XX:ThreadPriorityPolicy=42"` |  |
| jvmOptions[30] | string | `"-XX:+UseGCLogFileRotation"` |  |
| jvmOptions[31] | string | `"-XX:NumberOfGCLogFiles=10"` |  |
| jvmOptions[32] | string | `"-XX:GCLogFileSize=10M"` |  |
| jvmOptions[3] | string | `"-XX:+HeapDumpOnOutOfMemoryError"` |  |
| jvmOptions[4] | string | `"-Xss256k"` |  |
| jvmOptions[5] | string | `"-XX:StringTableSize=1000003"` |  |
| jvmOptions[6] | string | `"-XX:+AlwaysPreTouch"` |  |
| jvmOptions[7] | string | `"-XX:-UseBiasedLocking"` |  |
| jvmOptions[8] | string | `"-XX:+UseTLAB"` |  |
| jvmOptions[9] | string | `"-XX:+ResizeTLAB"` |  |
| livenessProbe | object | `{}` |  |
| metrics.enabled | bool | `false` |  |
| metrics.port | int | `9100` |  |
| metrics.serviceMonitor.additionalLabels | object | `{}` |  |
| metrics.serviceMonitor.enabled | bool | `false` |  |
| metrics.serviceMonitor.interval | string | `"30s"` |  |
| metrics.serviceMonitor.namespace | string | `"monitoring"` |  |
| nameOverride | string | `""` | Overrides the chart name |
| nativeTransportPort | int | `9042` |  |
| nodeSelector | object | `{}` |  |
| pdb.enabled | bool | `false` | Specifies whether a pod disruption budget should be created |
| persistence.accessModes[0] | string | `"ReadWriteOnce"` |  |
| persistence.enabled | bool | `false` |  |
| persistence.fixPermissions | bool | `false` |  |
| persistence.size | string | `"8Gi"` |  |
| persistence.storageClassName | string | `""` |  |
| persistence.volumeMode | string | `"Filesystem"` |  |
| podAnnotations | object | `{}` |  |
| podLabels | object | `{}` |  |
| podManagementPolicy | string | `"OrderedReady"` |  |
| podSecurityContext | object | `{"fsGroup":999}` | Pod security settings |
| podSecurityPolicy.annotations | object | `{}` |  |
| podSecurityPolicy.create | bool | `true` | Specifies whether a pod security policy should be created |
| podSecurityPolicy.enabled | bool | `true` | Specifies whether a pod security policy should be enabled |
| podSecurityPolicy.name | string | `""` | The name of the pod security policy to use. If not set and create is true, a name is generated using the fullname template |
| preStopHook | object | `{}` |  |
| priority | int | `0` |  |
| priorityClassName | string | `""` | Overrides default priority class ref: https://kubernetes.io/docs/concepts/configuration/pod-priority-preemption/ |
| rack | string | `"rack1"` |  |
| rbac.create | bool | `true` | Specifies whether a cluster role should be created |
| rbac.name | string | `""` | The name of the cluster role to use. If not set and create is true, a name is generated using the fullname template |
| readinessProbe | object | `{}` |  |
| replicaCount | int | `1` | Replicas count |
| resources.limits.cpu | int | `2` |  |
| resources.limits.memory | string | `"12Gi"` |  |
| resources.requests.cpu | int | `1` |  |
| resources.requests.memory | string | `"8Gi"` |  |
| rpcPort | int | `9160` |  |
| runtimeClassName | string | `""` | Overrides default runtime class |
| schedulerName | string | `""` |  |
| securityContext | object | `{"readOnlyRootFilesystem":false,"runAsUser":999}` | Contaier security settings |
| seedCount | int | `1` |  |
| service.annotations | object | `{}` | Annotations for Service resource |
| service.clusterIP | string | `""` | Exposes the Service on a cluster IP ref: https://kubernetes.io/docs/concepts/services-networking/service/#choosing-your-own-ip-address |
| service.externalTrafficPolicy | string | `"Cluster"` | If you set service.spec.externalTrafficPolicy to the value Local, kube-proxy only proxies proxy requests to local endpoints, and does not forward traffic to other nodes. This approach preserves the original source IP address. If there are no local endpoints, packets sent to the node are dropped, so you can rely on the correct source-ip in any packet processing rules you might apply a packet that make it through to the endpoint. ref: https://kubernetes.io/docs/tutorials/services/source-ip/#source-ip-for-services-with-type-nodeport |
| service.ipFamily | string | `""` | Address family for the Service's cluster IP (IPv4 or IPv6) ref: https://kubernetes.io/docs/concepts/services-networking/dual-stack/ |
| service.nativeTransportNodePort | int | `30042` |  |
| service.nativeTransportPort | int | `9042` |  |
| service.rpcNodePort | int | `30160` |  |
| service.rpcPort | int | `9160` |  |
| service.storageNodePort | int | `30170` |  |
| service.storageNodePortSsl | int | `30171` |  |
| service.storagePort | int | `7000` |  |
| service.storagePortSsl | int | `7001` |  |
| service.topology | bool | `false` | enables a service to route traffic based upon the Node topology of the cluster ref: https://kubernetes.io/docs/concepts/services-networking/service-topology/#using-service-topology Kubernetes >= kubeVersion 1.18 |
| service.topologyKeys | list | `[]` |  |
| service.type | string | `"ClusterIP"` | Kubernetes ServiceTypes allow you to specify what kind of Service you want ref: https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types |
| serviceAccount.annotations | object | `{}` | Annotations to add to the service account |
| serviceAccount.create | bool | `true` | Specifies whether a service account should be created |
| serviceAccount.name | string | `""` | The name of the service account to use. If not set and create is true, a name is generated using the fullname template |
| startupProbe | object | `{}` |  |
| storagePort | int | `7000` |  |
| storagePortSsl | int | `7001` |  |
| terminationGracePeriodSeconds | int | `30` | Grace period before the Pod is allowed to be forcefully killed ref: https://kubernetes.io/docs/concepts/containers/container-lifecycle-hooks/ |
| tolerations | list | `[]` |  |
| updateStrategy | object | `{}` |  |

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.kube-ops.io | generate | ~0.2.3 |

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.4.0](https://github.com/norwoodj/helm-docs/releases/v1.4.0)
